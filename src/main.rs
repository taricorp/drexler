#![feature(while_let, if_let, slicing_syntax)]

extern crate arena;

use arena::TypedArena;
use std::collections::HashMap;
use std::str::{MaybeOwned, Slice};
use token::Token;

mod token;

enum Register {
    /// 8-bit registers A, B, C, D, E, F, H and L
    Reg8(char),
    /// Interrupt vector register.
    RegI,
    /// Refresh register.
    RegR,

    /// BC, DE or HL, eg `Reg16('b')` for BC.
    Reg16(char),
    /// AF register pair (legal only for push/pop).
    RegAF,
    /// IX or IY, eg `RegIdx('x')` for IX.
    RegIdx(char),
    /// Stack pointer, legal in only a few locations.
    RegSP,
}

fn main() {
    // Blob of normalized source code. An arena is used here because it's
    // append-only, guaranteeing references taken from it will remain valid
    // even if we mutate it.
    let sources = TypedArena::<String>::new();

    let source = sources.alloc(" abc123,bar,baz (quux)".to_string());
    let mut tokens = token::source_to_tokens(source.as_slice());
    preprocess(&mut tokens);
    println!("{}", tokens);
}

type Macro<'a> = &'a str;
type MacroTable<'a> = HashMap<String, Macro<'a>>;

fn preprocess<'a>(tokens: &mut Vec<Token<'a>>) {
    let mut macros: MacroTable = HashMap::new();
    // Test
    //macros.insert("bar".into_string(), "bap");

    enum State {
        Null,
        NotDirective,
        GotHash
    }

    let mut state: State = Null;
    let mut iter = tokens.iter_mut();
    while let Some(token) = iter.next() {
        // Directives are always of the form
        //   '#' Whitespace()? Word(directive)
        // and must be at the beginning of a line, preceded only by whitespace.
        state = match state {
            Null => {
                match token {
                    &token::Octothorpe => GotHash,
                    &token::Indent => Null,
                    &token::Word(ref mut s) => {
                        maybe_expand_macro(s, &macros);
                        NotDirective
                    }
                    _ => NotDirective
                }
            }

            NotDirective => {
                match token {
                    &token::Word(ref mut s) => {
                        maybe_expand_macro(s, &macros);
                        NotDirective
                    }
                    _ => NotDirective,
                }
            }

            GotHash => {
                match token {
                    &token::Word(ref s) => {
                        handle_directive(s.as_slice(), &mut iter, &mut macros);
                        Null
                    }
                    _ => {
                        fail!("`{}' is not a valid directive.", token);
                    }
                }
            }
        }
    }
}

fn handle_directive<'a, 'b, I: Iterator<&'b mut Token<'a>>>(name: &str, iter: &mut I, macros: &MacroTable<'a>) {
    unimplemented!();
}


fn maybe_expand_macro<'a>(candidate: &mut MaybeOwned<'a>, macros: &MacroTable<'a>) {
    // TODO macros in macro expansions should also be expanded.
    // Example:
    // ```
    // foo
    // #define foo bar
    // #define bar baz
    // foo
    // ```
    // Should become:
    // ```
    // foo
    // baz
    // ```
    match macros.find_equiv(&candidate.as_slice()) {
        Some(replacement) => {
            *candidate = Slice(*replacement);
        }
        None => {}
    }
}
