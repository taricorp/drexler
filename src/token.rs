//! Source code tokenization.

use std;
use std::str::{MaybeOwned, Slice};

#[deriving(Show, PartialEq, Eq)]
pub enum Token<'a> {
    Comma,
    LParen,
    RParen,
    Plus,
    Minus,
    Slash,
    Asterisk,
    Quote,
    Apostrophe,
    EndOfLine,
    LCaret,
    RCaret,
    Octothorpe,
    /// Consecutive block of whitespace
    Whitespace(&'a str),
    /// Consecutive block of letters and numbers
    // TODO Might not need MaybeOwned, but it makes hacking on things easier.
    Word(MaybeOwned<'a>),

    /// Whitespace at the beginning of a line
    /// 
    /// These are generated only by `normalize_whitespace`.
    Indent,
    /// Preprocessor replacement for multiple tokens.
    Injection(Vec<Token<'a>>),
}

/// Maps a single character to the corresponding standalone token (if any).
///
/// Standalone tokens are those with no internal data, such as `Comma` or
/// `EndOfLine`.
fn single_char_token<'a>(c: char) -> Option<Token<'a>> {
    Some(match c {
        ',' => Comma,
        '(' => LParen,
        ')' => RParen,
        '+' => Plus,
        '-' => Minus,
        '/' => Slash,
        '*' => Asterisk,
        '"' => Quote,
        '\'' => Apostrophe,
        '\n' => EndOfLine,
        '<' => LCaret,
        '>' => RCaret,
        '#' => Octothorpe,
        _ => return None
    })
}

/// Transforms a string of source code into a collection of `Token`s.
pub fn source_to_tokens<'a>(source: &'a str) -> Vec<Token<'a>> {
    let mut chars = source.chars().map(|c| (CharacterClass::classify(c), c)).enumerate().peekable();
    let mut tokens: Vec<Token<'a>> = Vec::new();

    // For loop in disguise, working around some lexical borrow issues with for loops.
    while let Some((idx, (class, character))) = chars.next() {
        let token: Token<'a> = match class {
            SingleClass => single_char_token(character).unwrap(),
            WordClass => {
                let len = consume_while(&mut chars, |&(_, (class, _))| {
                    match class {
                        WordClass => true,
                        _ => false
                    }
                });
                // TODO slice_chars is slow. Either need a [char] or byte indices.
                Word(Slice(source.slice_chars(idx, idx + len + 1)))
            }
            WhitespaceClass => {
                let len = consume_while(&mut chars, |&(_, (class, _))| {
                    match class {
                        WhitespaceClass => true,
                        _ => false
                    }
                });
                // TODO slice_chars is slow. Either need a [char] or byte indices.
                Whitespace(source.slice_chars(idx, idx + len + 1))
            }
        };
        tokens.push(token);
    }
    normalize_whitespace(tokens)
}

#[test]
fn test_source_to_tokens() {
    let s = "abc 123\n <foo,bar>(baz   quux)".to_string();
    assert_eq!(source_to_tokens(s.as_slice()),
               vec![Word(Slice("abc")), Word(Slice("123")), EndOfLine,
                    Indent, LCaret, Word(Slice("foo")), Comma, Word(Slice("bar")), RCaret,
                    LParen, Word(Slice("baz")), Word(Slice("quux")), RParen]);
}

/// Strips whitespace from the token stream except inside string literals.
///
/// Replaces whitespace at the beginning of lines with `Indent`, keeps it
/// when bracketed by `Quote` or `Apostrophe`, and deletes everything else.
fn normalize_whitespace<'a>(tokens: Vec<Token<'a>>) -> Vec<Token<'a>> {
    enum State {
        Bol,
        Apos,
        Quot,
        Null
    };
    let mut state = Bol;

    tokens.into_iter().filter_map(|token| {
        let mut replacement = None::<Token<'a>>;

        let (next, take) = match (state, &token) {
            (Null, &EndOfLine) => (Bol, true),
            (Null, &Apostrophe) => (Apos, true),
            (Null, &Quote) => (Quot, true),
            (Null, &Whitespace(_)) => (Null, false),
            (Null, _) => (Null, true),

            (Apos, &Apostrophe) => (Null, true),
            (Apos, _) => (Apos, true),

            (Quot, &Quote) => (Null, true),
            (Quot, _) => (Quot, true),

            (Bol, &Whitespace(_)) => {
                replacement = Some(Indent);
                (Null, false)
            }
            (Bol, _) => (Null, true)
        };
        state = next;
        if take {
            Some(token)
        } else {
            replacement
        }
    }).collect::<Vec<Token>>()
}


/// Preliminary classification of characters into tokens.
///
/// During tokenization, the first pass determines classification of characters,
/// and the second pass collects classes into full tokens. `SingleClass` are
/// not collapsed, while adjacent characters of the same class are combined
/// into a single token.
enum CharacterClass {
    WhitespaceClass,
    WordClass,
    SingleClass,
}

impl CharacterClass {
    /// Gets the class which the given character is a member of.
    fn classify(c: char) -> CharacterClass {
        match c {
            ' ' | '\t' => WhitespaceClass,
            x if single_char_token(x).is_some() => SingleClass,
            _ => WordClass
        }
    }
}

#[test]
fn test_classify_character() {
    assert!(match CharacterClass::classify(' ') {
        WhitespaceClass => true,
        _ => false
    });
    assert!(match CharacterClass::classify(',') {
        SingleClass => true,
        _ => false
    });
    assert!(match CharacterClass::classify('a') {
        WordClass => true,
        _ => false
    });
}

/// Consumes items from an iterator as long as the predicate returns true.
/// 
/// The next item returned by the iterator will be the first one which does
/// not match this predicate; there are no lost items (unlike
/// `Iterator::take_while`).
fn consume_while<T, I: Iterator<T>>(iter: &mut std::iter::Peekable<T, I>, pred: |&T| -> bool) -> uint {
    let mut count = 0u;
    loop {
        match iter.peek() {
            Some(x) => {
                if !pred(x) {
                    break;
                }
            }
            None => {
                break;
            }
        }
        iter.next();
        count += 1;
    }
    return count;
}

#[test]
fn test_consume_while() {
    let foo = [1u, 2, 3, 4, 3, 2, 1];
    let mut iter = foo.iter().peekable();

    {
        assert_eq!(consume_while(&mut iter, |&&x| x < 4), 3);
    }
    let x = *iter.next().unwrap();
    assert_eq!(x, 4u);
}
